var express = require('express');
var router = express.Router();

var assets = {CreditCard :[{"IssuingNetwork": "American Express", "CardNumber": 375283401434484}]}




router.get('/', function(req, res){

    res.render("index",{title:'Credit Cards',items:assets.CreditCard ,numA :{"NumAssets":assets.CreditCard.length}});

});



router.route('/cnc/assets').get(function(req, res) {

    assets.CreditCard=[];

    res.redirect('/');

}).post(function(req, res){

    var newCardName = req.body.newCardName;
    var newCardNum = req.body.newCardNum;
    assets.CreditCard.push({
        IssuingNetwork: newCardName,
        CardNumber: newCardNum
    });
    res.redirect('/');
});



console.log('Server running at http://127.0.0.1:5000/');

module.exports = router;
